import 'dart:convert';

class Mahasiswa{
  int id;
  String nama;
  String nim;
  String email;
  String jenis_kelamin;

  Mahasiswa({
    this.id = 0,
    this.nama,
    this.nim,
    this.email,
    this.jenis_kelamin
  });

  factory Mahasiswa.fromJson(Map<String,dynamic>map){
    return Mahasiswa(
      id:map["id"],
      nama:map["nama"],
      email:map["email"],
      jenis_kelamin:map["jenis_kelamin"]
    );
  }

  Map<String,dynamic> toJson(){
    return {
      "id":id,
      "nama":nama,
      "nim":nim,
      "email":email,
      "jenis_kelamin":jenis_kelamin,
    };
  }

  @override
  String toString(){
    return 'Mahasiswa{id:$id,nama:$nama,nim:$nim,email:$email,jenis_kelamin:$jenis_kelamin';
  } 
}

List<Mahasiswa> mahasiswaFromJson(String jsonData){
  final data = json.decode(jsonData);
  return List<Mahasiswa>.from(data.map((item)=>
  Mahasiswa.fromJson(item)));
}

String mahasiswaToJson(Mahasiswa data){
  final jsonData = data.toJson();
  return json.encode(jsonData);
}